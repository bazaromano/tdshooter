// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShooterGameMode.h"
#include "TDShooterPlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "TDShooter/Character/TDShooterCharacter.h"

ATDShooterGameMode::ATDShooterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDShooterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}